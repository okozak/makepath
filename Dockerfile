FROM python:3.5.2

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt
