MakePath
========

MakePath is a small library which aims to facilitate paths building from various locations.

MakePath is released under the `LGPL v3 license <https://www.gnu.org/licenses/lgpl-3.0.en.html>`_.

Installation
------------

Here is how to install MakePath from PyPI :

.. code-block:: bash

    pip install makepath

How to use it
-------------

Here is how to make a path from root :

.. code-block:: python

    >>> import makepath
    >>> makepath.from_root("usr", "local", "bin")
    '/usr/local/bin'

You can of course make paths from other locations. See below for more information.

The makepath module
-------------------

This module contains all the helper functions used to make paths.

.. module:: makepath

.. py:function:: from_root

   This function is used to make paths from root :

   .. code-block:: python

       >>> import makepath
       >>> makepath.from_root("usr", "local", "bin")
       '/usr/local/bin'

.. py:function:: from_home

   This function is used to make paths from home.

   Considering the following user's home :

   .. code-block:: python

       >>> import os
       >>> os.path.expanduser("~")
       '/root'

   Here is how to make a path from it :

   .. code-block:: python

       >>> import makepath
       >>> makepath.from_home(".virtualenvs", "makepath", "bin")
       '/root/.virtualenvs/makepath/bin'

.. py:function:: from_working_dir

   This function is used to make paths from the working directory.

   Considering the following working directory :

   .. code-block:: python

       >>> import os
       >>> os.path.abspath('.')
       '/makepath'

   Here is how to make a path from it :

   .. code-block:: python

       >>> import makepath
       >>> makepath.from_working_dir("tests", "makepath", "__init__.py")
       '/makepath/tests/makepath/__init__.py'

.. py:function:: from_this_file

   This function is used to make paths from the directory containing the file from where the code is written.
