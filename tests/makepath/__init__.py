#
#   Copyright 2016 Olivier Kozak
#
#   This file is part of MakePath.
#
#   MakePath is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   MakePath is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
#   details.
#
#   You should have received a copy of the GNU Lesser General Public License along with MakePath. If not, see
#   <http://www.gnu.org/licenses/>.
#

from assertpy import assert_that

import makepath


def test_from_root():
    path = makepath.from_root("usr", "local", "bin")
    assert_that(path).is_equal_to("/usr/local/bin")


def test_from_home():
    path = makepath.from_home(".virtualenvs", "makepath", "bin")
    assert_that(path).is_equal_to("/root/.virtualenvs/makepath/bin")


def test_from_working_dir():
    path = makepath.from_working_dir("tests", "makepath", "__init__.py")
    assert_that(path).is_equal_to("/makepath/tests/makepath/__init__.py")


def test_from_this_file():
    path = makepath.from_this_file("..", "..", "tests", "makepath", "__init__.py")
    assert_that(path).is_equal_to("/makepath/tests/makepath/__init__.py")
