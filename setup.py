#
#   Copyright 2016 Olivier Kozak
#
#   This file is part of MakePath.
#
#   MakePath is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
#   version.
#
#   MakePath is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
#   details.
#
#   You should have received a copy of the GNU Lesser General Public License along with MakePath. If not, see
#   <http://www.gnu.org/licenses/>.
#

import os

import ilio
import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="MakePath",
        use_scm_version=True,
        description="A small library which aims to facilitate paths building from various locations",
        long_description=ilio.read(os.path.join(os.path.dirname(__file__), "README.rst")),
        license="GNU LGPL v3",
        author="Olivier Kozak",
        author_email="olivier.kozak@gmail.com",
        packages=["makepath"],
        setup_requires=['setuptools_scm']
    )
